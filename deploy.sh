echo "STOPPING OLD APP"
sudo fuser -k 80/tcp
sudo rm -rf web-development
echo "CLONNING REPO"
git clone --branch master  https://gitlab.com/olehhaliak/web-development.git
echo "BUILDING"
chmod +x web-development/mvnw
cd web-development/
./mvnw clean install
echo "RUNNING"
sudo java -jar -Dspring.profiles.active=standalone target/certycheck-0.0.1-SNAPSHOT.jar >>out.log 2>>err.log &
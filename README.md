# CertyCheck

This project intended to solve the problem of fake certificates, that students often use to gain some extra points  

---
### Explore API
  - The app is deployed at `http://18.197.172.83/`
  - Visit [swagger-ui](http://18.197.172.83/swagger-ui/#/certificate-controller) to see available commands
---
### Setup & Run
##### Requirements:
  - JDK 11 or later
  - Maven
  - Postgres (optional)

##### Run 
  1. Clone repository, open project:  
     ```
     git clone https://gitlab.com/olehhaliak/web-development.git
     cd web-development/
     ```
  2. Build project with Maven:  
     ```
     mvn clean install
     ```
  3. Configure DB connection (optional):  
     - Open `src/main/resources/application.properties`
     - Overwrite `spring.datasource.url`, `spring.datasource.username`, and `spring.datasource.password` properties
  4. Run project from jar:  
     ```
     java -jar target/certycheck-0.0.1-SNAPSHOT.jar
     ```
     If you skipped step 3, you should run project in standalone mode:  
     ``` 
     java -jar -Dspring.profiles.active=standalone target/certycheck-0.0.1-SNAPSHOT.jar
     ``` 

---
#### Used technologies & tools
  - Spring boot - core framework
  - Spring data JPA - for connection to DB
  - Lombok - library for reducing boilerplate code
  - Junit - unit test framework
  - Mockito - mock library
  - Maven - build tool & unit test runner
  - Postgresql - main database
  - H2 - test database (also acts as a main DB in standalone mode)
  - Gitlab CI - CI\CD tool
  - AWS EC2 - service that hosts CD env

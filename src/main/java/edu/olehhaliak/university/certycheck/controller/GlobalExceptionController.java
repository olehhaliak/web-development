package edu.olehhaliak.university.certycheck.controller;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import springfox.documentation.annotations.ApiIgnore;

import javax.annotation.PostConstruct;

@ApiIgnore
@ControllerAdvice
@Slf4j
public class GlobalExceptionController {
    @Value("${debug.exceptions.printDetailedMessage:false}")
    boolean printDetails;

    @ExceptionHandler(value = {Exception.class})
    public ResponseEntity<Object> handleException(Exception ex) {
        log.error(ex.getMessage());
        ex.printStackTrace();
        return new ResponseEntity<>(printDetails ? ex.getMessage() : null, HttpStatus.INTERNAL_SERVER_ERROR);
    }

    @PostConstruct
    public void init() {
        if (printDetails) {
            log.warn("debug.exceptions.printDetailedMessage was turned on. Now any exception will be returned in " +
                    "response if any unexpected exception occur.");
        }
    }

}

package edu.olehhaliak.university.certycheck.controller;

import edu.olehhaliak.university.certycheck.api.CertificateApi;
import edu.olehhaliak.university.certycheck.dto.RegisterCertificateRequestDto;
import edu.olehhaliak.university.certycheck.dto.RegisterCertificateResponseDto;
import edu.olehhaliak.university.certycheck.dto.UpdateCertificateRequestDto;
import edu.olehhaliak.university.certycheck.model.Certificate;
import edu.olehhaliak.university.certycheck.service.CertificateService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.websocket.server.PathParam;


@RestController("/certificate")
@RequestMapping("/certificate")
@RequiredArgsConstructor
public class CertificateController implements CertificateApi {
    final CertificateService certificateService;

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public RegisterCertificateResponseDto registerCertificate(@RequestBody RegisterCertificateRequestDto certificateDto) {
        Long certificateId = certificateService.registerCertificate(certificateDto);
        return new RegisterCertificateResponseDto(certificateId);
    }

    @GetMapping("/{certificateId}")
    public ResponseEntity<Certificate> verifyCertificate(@PathVariable("certificateId") Long certificateId) {
        return ResponseEntity.of(certificateService.getCertificate(certificateId));
    }

    @PutMapping("/{certificateId}")
    public ResponseEntity<Certificate> updateCertificate(@PathVariable("certificateId") Long certificateId,
                                                         @RequestBody UpdateCertificateRequestDto dto) {
        if (certificateService.certificateExists(certificateId)) {
            return new ResponseEntity<>(certificateService.updateCertificate(certificateId, dto), HttpStatus.OK);
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @DeleteMapping("/{certificateId}")
    public ResponseEntity<Void> deleteCertificate(@PathVariable("certificateId") Long certificateId){
        if(certificateService.certificateExists(certificateId)){
            certificateService.deleteCertificate(certificateId);
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

}
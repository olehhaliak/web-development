package edu.olehhaliak.university.certycheck.model;

import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import java.util.Date;

@Data
@Entity
public class Certificate {
    @Id
    @GeneratedValue
    private Long id;
    private String name;
    private String holder;
    private String issuer;
    /* Refers to the date certificate was initially created */
    private Date dateIssued;
    /* Refers to the date certificate was registered on CertyCheck platform */
    private Date dateRegistered;

}

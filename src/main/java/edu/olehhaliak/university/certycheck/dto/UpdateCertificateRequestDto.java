package edu.olehhaliak.university.certycheck.dto;

import lombok.Data;

@Data
public class UpdateCertificateRequestDto {

    private String name;
    private String issuer;
    private String holder;
}

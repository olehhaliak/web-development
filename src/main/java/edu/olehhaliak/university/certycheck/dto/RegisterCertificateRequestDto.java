package edu.olehhaliak.university.certycheck.dto;

import lombok.Data;

import java.util.Date;

@Data
public class RegisterCertificateRequestDto {

    private String name;
    private String issuer;
    private String holder;
    private Date dateIssued;

}

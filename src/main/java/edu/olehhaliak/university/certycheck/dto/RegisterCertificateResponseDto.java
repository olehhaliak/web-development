package edu.olehhaliak.university.certycheck.dto;

import lombok.Data;


@Data
public class RegisterCertificateResponseDto {
    public static final String VERIFICATION_LINK_PATH = "/certificate/";

    public RegisterCertificateResponseDto(Long certificateId) {
        this.certificateId = certificateId;
        this.verificationLink = VERIFICATION_LINK_PATH + certificateId;
    }

    private Long certificateId;
    private String verificationLink;
}
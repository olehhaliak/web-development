package edu.olehhaliak.university.certycheck.Repository;

import edu.olehhaliak.university.certycheck.model.Certificate;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CertificateRepository extends JpaRepository<Certificate,Long> {
}

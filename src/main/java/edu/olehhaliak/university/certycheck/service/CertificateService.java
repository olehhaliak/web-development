package edu.olehhaliak.university.certycheck.service;

import edu.olehhaliak.university.certycheck.Repository.CertificateRepository;
import edu.olehhaliak.university.certycheck.dto.RegisterCertificateRequestDto;
import edu.olehhaliak.university.certycheck.dto.UpdateCertificateRequestDto;
import edu.olehhaliak.university.certycheck.model.Certificate;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.Optional;

//@TODO: separate abstraction and implementation
@Service
@RequiredArgsConstructor
public class CertificateService {
    final CertificateRepository certificateRepository;


    public Long registerCertificate(RegisterCertificateRequestDto dto) {
        Certificate certificate = new Certificate();
        BeanUtils.copyProperties(dto, certificate);
        certificate.setDateRegistered(new Date());
        if (certificate.getDateIssued() == null) {
            certificate.setDateIssued(certificate.getDateRegistered());
        }
        return certificateRepository.save(certificate).getId();
    }

    public Optional<Certificate> getCertificate(Long certificateId) {
        return certificateRepository.findById(certificateId);
    }

    public boolean certificateExists(Long certificateId) {
        return certificateRepository.existsById(certificateId);
    }


    public Certificate updateCertificate(Long certificateId, UpdateCertificateRequestDto dto) {
        Certificate certificate = certificateRepository.findById(certificateId).get();
        certificate.setName(dto.getName());
        certificate.setIssuer(dto.getIssuer());
        certificate.setHolder(dto.getHolder());
        certificateRepository.save(certificate);
        return certificate;
    }

    public void deleteCertificate(Long certificateId) {
        certificateRepository.deleteById(certificateId);
    }

}
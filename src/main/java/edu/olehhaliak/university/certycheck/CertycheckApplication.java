package edu.olehhaliak.university.certycheck;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CertycheckApplication {

    public static void main(String[] args) {
        SpringApplication.run(CertycheckApplication.class, args);
    }

}

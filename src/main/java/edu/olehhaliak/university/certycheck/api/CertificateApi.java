package edu.olehhaliak.university.certycheck.api;


import edu.olehhaliak.university.certycheck.dto.RegisterCertificateRequestDto;
import edu.olehhaliak.university.certycheck.dto.RegisterCertificateResponseDto;
import edu.olehhaliak.university.certycheck.model.Certificate;
import io.swagger.annotations.ApiOperation;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;

public interface CertificateApi {
    @ApiOperation(value = "register certificate",notes = "Registers new certificate")
    RegisterCertificateResponseDto registerCertificate(RegisterCertificateRequestDto certificateDto);

    @ApiOperation(value = "verify certificate", notes = "Returns a certificate if it exists")
    ResponseEntity<Certificate> verifyCertificate(@PathVariable("certificateId") Long certificateId);
}

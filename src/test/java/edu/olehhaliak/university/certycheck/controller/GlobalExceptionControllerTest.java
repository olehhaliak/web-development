package edu.olehhaliak.university.certycheck.controller;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.springframework.http.HttpStatus;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.util.ReflectionTestUtils;

import static org.junit.jupiter.api.Assertions.*;

@ExtendWith(SpringExtension.class)
class GlobalExceptionControllerTest {
    @InjectMocks
    GlobalExceptionController controller;

    private static final String ERR_MESSAGE = "virtually any error message";
    private final Exception mockException = new ArithmeticException(ERR_MESSAGE);

    @Test
    void printDetailsOn_FullMessageReturned() {
        ReflectionTestUtils.setField(controller, "printDetails", true);
        assertEquals(ERR_MESSAGE, controller.handleException(mockException).getBody());
    }

    @Test
    void printDetailsOff_NoErrMessageReturned() {
        ReflectionTestUtils.setField(controller, "printDetails", false);
        assertNull(controller.handleException(mockException).getBody());
    }

    @Test
    void responseCodeIs500() {
        assertEquals(HttpStatus.INTERNAL_SERVER_ERROR, controller.handleException(mockException).getStatusCode());
    }
}
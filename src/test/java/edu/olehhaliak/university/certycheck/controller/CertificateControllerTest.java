package edu.olehhaliak.university.certycheck.controller;

import edu.olehhaliak.university.certycheck.dto.RegisterCertificateRequestDto;
import edu.olehhaliak.university.certycheck.dto.RegisterCertificateResponseDto;
import edu.olehhaliak.university.certycheck.dto.UpdateCertificateRequestDto;
import edu.olehhaliak.university.certycheck.model.Certificate;
import edu.olehhaliak.university.certycheck.service.CertificateService;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.*;


@ExtendWith(SpringExtension.class)
class CertificateControllerTest {
    @InjectMocks
    CertificateController certificateController;

    @Mock
    CertificateService mockCertificateService;

    @Mock
    Certificate mockCertificate;

    @Mock
    RegisterCertificateRequestDto registerCertificateRequestDto;

    final Long mockCertId = 15L;

    @Nested
    class VerifyCertificate {
        @Test
        void certificateExists_isReturned() {
            when(mockCertificateService.getCertificate(mockCertId)).thenReturn(Optional.of(mockCertificate));
            ResponseEntity<Certificate> actualCertificate = certificateController.verifyCertificate(mockCertId);
            assertEquals(mockCertificate, actualCertificate.getBody());
        }

        @Test
        void certificateExists_ResponseCodeIs200() {
            when(mockCertificateService.getCertificate(mockCertId)).thenReturn(Optional.of(mockCertificate));
            ResponseEntity<Certificate> actualCertificate = certificateController.verifyCertificate(mockCertId);
            assertEquals(HttpStatus.OK, actualCertificate.getStatusCode());
        }

        @Test
        void certificateNotExists_ResponseCodeIs404() {
            when(mockCertificateService.getCertificate(mockCertId)).thenReturn(Optional.empty());
            ResponseEntity<Certificate> actualRespEntity = certificateController.verifyCertificate(mockCertId);
            assertEquals(HttpStatus.NOT_FOUND, actualRespEntity.getStatusCode());
        }
    }

    @Nested
    class DeleteCertificate{
        @Test
        void certificateExists_isDeleted() {
            when(mockCertificateService.certificateExists(1L)).thenReturn(true);
            certificateController.deleteCertificate(1L);
            verify(mockCertificateService).deleteCertificate(1L);
        }

        @Test
        void certificateExists_ResponceCode204Returned() {
            when(mockCertificateService.certificateExists(1L)).thenReturn(true);
            assertEquals(HttpStatus.NO_CONTENT,certificateController.deleteCertificate(1L).getStatusCode());
        }

        @Test
        void certificateNotExist_ResponceCode404Returned() {
            when(mockCertificateService.certificateExists(1L)).thenReturn(false);
            assertEquals(HttpStatus.NOT_FOUND,certificateController.deleteCertificate(1L).getStatusCode());
        }
    }

    @Nested
    class UpdateCertificate {
        @Test
        void certificateUpdated_ResponseCode200Returned() {
            when(mockCertificateService.certificateExists(1L)).thenReturn(true);
            assertEquals(HttpStatus.OK,certificateController.updateCertificate(1L,mock(UpdateCertificateRequestDto.class)).getStatusCode());
        }

        @Test
        void certificateExists_BeingUpdated() {
            when(mockCertificateService.certificateExists(1L)).thenReturn(true);
            certificateController.updateCertificate(1L,mock(UpdateCertificateRequestDto.class));
            verify(mockCertificateService).updateCertificate(anyLong(),any());
        }

        @Test
        void certificateNotExist_ResponceCode404Returned() {
            when(mockCertificateService.certificateExists(1L)).thenReturn(false);
            assertEquals(
                    HttpStatus.NOT_FOUND,
                    certificateController.updateCertificate(1L,mock(UpdateCertificateRequestDto.class)).getStatusCode());
        }
    }

    @Test
    void registerCertificateTest() {
        when(mockCertificateService.registerCertificate(registerCertificateRequestDto)).thenReturn(mockCertId);
        RegisterCertificateResponseDto responseDto = certificateController.registerCertificate(registerCertificateRequestDto);
        assertEquals(mockCertId, responseDto.getCertificateId());
        assertEquals("/certificate/" + mockCertId, responseDto.getVerificationLink());
    }
}


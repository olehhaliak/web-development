package edu.olehhaliak.university.certycheck.service;

import edu.olehhaliak.university.certycheck.Repository.CertificateRepository;
import edu.olehhaliak.university.certycheck.dto.RegisterCertificateRequestDto;
import edu.olehhaliak.university.certycheck.dto.UpdateCertificateRequestDto;
import edu.olehhaliak.university.certycheck.model.Certificate;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.*;

@ExtendWith(SpringExtension.class)
class CertificateServiceTest {
    @InjectMocks
    CertificateService certificateService;

    @Mock
    CertificateRepository certificateRepository;

    @Mock
    Certificate mockCertificate;

    @Mock
    RegisterCertificateRequestDto mockCertificateDto;

    final Long mockCertificateId = 15L;


    @Nested
    class GetCertificate {
        @Test
        void certificateExists_isReturned() {
            when(certificateRepository.findById(mockCertificateId)).thenReturn(Optional.of(mockCertificate));
            Optional<Certificate> actualCertificateOption = certificateService.getCertificate(mockCertificateId);
            assertTrue(actualCertificateOption.isPresent());
            assertSame(mockCertificate, certificateService.getCertificate(mockCertificateId).get());
        }

        @Test
        void certificateNotExist_EmptyOptionalReturned() {
            when(certificateRepository.findById(anyLong())).thenReturn(Optional.empty());
            assertTrue(certificateService.getCertificate(1L).isEmpty());
        }
    }

    @Nested
    class RegisterCertificate {
        @Test
        void registerCertificateTest() {
            when(certificateRepository.save(any())).thenReturn(mockCertificate);
            when(mockCertificate.getId()).thenReturn(mockCertificateId);
            assertEquals(mockCertificateId, certificateService.registerCertificate(mockCertificateDto));
        }

        @Test
        void dateIssuedNotSpecified_SetSameAsDateRegistered() {
            when(certificateRepository.save(any())).thenReturn(mockCertificate);
            when(mockCertificate.getId()).thenReturn(mockCertificateId);
            certificateService.registerCertificate(mockCertificateDto);
            verify(certificateRepository).save(argThat((Certificate cert) -> cert.getDateIssued().equals(cert.getDateRegistered())));
        }
    }

    @Nested
    class UpdateCertificate {
        UpdateCertificateRequestDto dto = new UpdateCertificateRequestDto();

        @BeforeEach
        void setUp() {
            dto.setName("abc");
            dto.setHolder("abc1");
            dto.setIssuer("abc2");
        }

        @Test
        void updatesFields() {
            when(certificateRepository.findById(anyLong())).thenReturn(Optional.of(new Certificate()));

            Certificate updatedCert = certificateService.updateCertificate(1L,dto);

            assertEquals("abc",updatedCert.getName());
            assertEquals("abc1",updatedCert.getHolder());
            assertEquals("abc2",updatedCert.getIssuer());
        }

        @Test
        void savesChanges() {
            Certificate mockCert = mock(Certificate.class);
            when(certificateRepository.findById(anyLong())).thenReturn(Optional.of(mockCert));
            Certificate updatedCert = certificateService.updateCertificate(1L,dto);
            verify(certificateRepository).save(any());
        }
    }

    @Nested
    class CertificateExists{
        @Test
        void certificateDoExists() {
            when(certificateRepository.existsById(1L)).thenReturn(true);
            assertTrue(certificateService.certificateExists(1L));
        }

        @Test
        void certificateDoesNotExists() {
            when(certificateRepository.existsById(1L)).thenReturn(false);
            assertFalse(certificateService.certificateExists(1L));
        }
    }

    @Test
    void deleteByIdTest() {
        certificateService.deleteCertificate(1L);
        verify(certificateRepository).deleteById(1L);
    }
}